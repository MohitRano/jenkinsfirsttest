﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(JenkinsFirstTest.Startup))]
namespace JenkinsFirstTest
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
